package com.justproperty.android.views.widget;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.login.widget.LoginButton;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.justproperty.android.R;
import com.justproperty.android.injections.DaggerInjector;
import com.justproperty.android.interfaces.LoginCancelListener;
import com.justproperty.android.prefs.SharedPreferenceManager;
import com.justproperty.android.presenters.LoginPresenter;
import com.justproperty.android.utils.Constants;
import com.justproperty.android.views.LoginView;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class LoginWidget extends DialogFragment implements LoginView, GoogleApiClient.OnConnectionFailedListener {

    private LoginPresenter presenter;

    @Bind(R.id.google_sigin_button)
    RelativeLayout siginView;

    @Bind(R.id.progress_bar)
    ProgressBar progressBar;

    @Bind (R.id.fb_login_button)
    LoginButton fbLoginButton;

    private LoginCancelListener loginCancelListener;

    @Inject
    SharedPreferenceManager sharedPreferenceManager;

    public static LoginWidget newInstance(LoginPresenter presenter) {
        LoginWidget frag = new LoginWidget();
        frag.presenter = presenter;
        presenter.setView(frag);
        return frag;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_signin_widget_dialog, container);
        DaggerInjector.inject(this);
        ButterKnife.bind(this, view);

        getDialog().requestWindowFeature(STYLE_NO_TITLE);

        setStyle(DialogFragment.STYLE_NO_TITLE, android.R.style.Theme_Holo_Light_Dialog_NoActionBar_MinWidth);

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        presenter.onViewCreated();

    }

    @Override
    public void onStart() {
        super.onStart();
        presenter.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
        presenter.onStop();
    }

    @OnClick (R.id.login_close_btn)
    void onCloseButtonClicked() {
        dismiss();
    }



    @OnClick(R.id.google_sigin_button)
    void onGooglePlusButtonClicked()
    {
        presenter.sendGoogleLoginRequest(this);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == Constants.REQUEST_CODE_GOOGLE_SIGN_IN) {
            presenter.onGoogleSigninResultReceive(data);
        } else {
            presenter.onFacebookSigninResultReceive(requestCode, resultCode, data);
        }
    }

    @Override
    public void showLoginError(String errorMsg) {

        if (isDetached() || getDialog() == null || getActivity() == null || getActivity().isFinishing()) {
            return;
        }

        View toastView = View.inflate(getActivity(), R.layout.toast_send_enquiry_result, null);

        ((TextView) toastView.findViewById(R.id.toast_header)).setText(R.string.login_error);
        ((TextView) toastView.findViewById(R.id.toast_desc)).setText(errorMsg);
        showToast(toastView);
    }

    @Override
    public void showProgressBar() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressBar() {
        if (isDetached() || getDialog() == null || getActivity() == null || getActivity().isFinishing()) {
            return;
        }
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void disableInteractions() {
        fbLoginButton.setEnabled(false);
        siginView.setEnabled(false);
    }

    @Override
    public void enableInteractions() {
        if (isDetached() || getDialog() == null || getActivity() == null || getActivity().isFinishing()) {
            return;
        }
        fbLoginButton.setEnabled(true);
        siginView.setEnabled(true);
    }

    @Override
    public void dismissDialog() {
        dismiss();
    }

    @OnClick(R.id.fb_login_button)
    public void onFbButtonClicked(LoginButton loginButton)


    {
        disableInteractions();
    }

    @Override
    public LoginButton getFbLoginButton() {
        return fbLoginButton;
    }

    private void showToast(View toastView) {
        Toast myToast = new Toast(getActivity());
        myToast.setGravity(Gravity.CENTER, 0, 0);
        myToast.setView(toastView);
        myToast.setDuration(Toast.LENGTH_LONG);
        myToast.show();
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        if (loginCancelListener == null) {
            return;
        }

        if (sharedPreferenceManager.getUserProfile() == null) {
            loginCancelListener.onLoginCanceled();
        }
    }

    public void setLoginCancelListener(LoginCancelListener loginListener) {
        this.loginCancelListener = loginListener;
    }
}
