package com.justproperty.android.presenters;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.facebook.login.LoginManager;
import com.facebook.login.widget.LoginButton;
import com.justproperty.android.injections.DaggerInjector;
import com.justproperty.android.interactor.LoginInteractor;
import com.justproperty.android.interactor.LoginInteractorImpl;
import com.justproperty.android.model.login_user.UserProfile;
import com.justproperty.android.utils.Constants;
import com.justproperty.android.utils.TagManagerInstance;
import com.justproperty.android.views.LoginView;

import javax.inject.Inject;

public class LoginPresenterImpl implements LoginPresenter {
    private static final String TAG = LoginPresenterImpl.class.getSimpleName();

    private LoginInteractor loginInteractor;
    private LoginView view;
    private Context context;

    @Inject
    TagManagerInstance tagManagerInstance;

    public LoginPresenterImpl(Context context) {

        DaggerInjector.inject(this);
        loginInteractor = new LoginInteractorImpl(context, this);
        this.context = context;

    }

    @Override
    public void setView(LoginView view) {
        this.view = view;
        Log.d(TAG, "setView");
    }

    @Override
    public void sendGoogleLoginRequest(Fragment fragment) {
        view.disableInteractions();
        view.showProgressBar();
        loginInteractor.sendGoogleLoginRequest(fragment);
    }

    @Override
    public void onGoogleSigninResultReceive(Intent intent) {
        loginInteractor.handleGoogleSigninResponse(intent);
    }

    @Override
    public void onFacebookSigninResultReceive(int requestCode, int resultCode, Intent intent) {
        view.disableInteractions();
        view.showProgressBar();
        loginInteractor.handleFacebookSigninResponse(requestCode, resultCode, intent);
    }

    private void sendSystemWideBroadcast(UserProfile userProfile) {

        LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(context);
        Intent logicBroadcastIntent = new Intent(Constants.LOGIN_BROADCAST_ACTION);
        logicBroadcastIntent.putExtra(UserProfile.class.getSimpleName(), userProfile);
        localBroadcastManager.sendBroadcast(logicBroadcastIntent);
    }

    @Override
    public void onLoginToServerSuccess(UserProfile userProfile) {

        tagManagerInstance.pushLoginSuccessEvent(userProfile.socialNetwork);
        loginInteractor.setUserToAppBoy(userProfile.id);
        sendSystemWideBroadcast(userProfile);
        view.hideProgressBar();
        view.dismissDialog();
    }

    @Override
    public void onLoginToServerFailed(String errorMsg) {
        view.hideProgressBar();
        view.enableInteractions();
        view.showLoginError(errorMsg);
    }

    @Override
    public void onStop() {
        loginInteractor.disconnectGoogleApiClient();
    }

    @Override
    public void onViewCreated() {
        loginInteractor.initializeGoogleSignin();
        setupFbButtonBehaviour();
    }

    @Override
    public void signOutUserFromGoogle(Context context, MenuNavigationPresenter menuNavigationPresenter) {
        loginInteractor.signOut(context, menuNavigationPresenter);

    }


    @Override
    public void onStart(){
        loginInteractor.connectToGoogleApi();
    }

    private void setupFbButtonBehaviour() {

        LoginButton fbLoginButton = view.getFbLoginButton();
        LoginManager.getInstance().logOut();
        fbLoginButton.setFragment((Fragment) view);
        loginInteractor.initializeFbSignin(fbLoginButton);
    }


}
