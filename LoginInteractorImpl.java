package com.justproperty.android.interactor;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.justproperty.android.R;
import com.justproperty.android.api.services.Api;
import com.justproperty.android.injections.DaggerInjector;
import com.justproperty.android.model.login_user.UserProfile;
import com.justproperty.android.prefs.SharedPreferenceManager;
import com.justproperty.android.presenters.LoginPresenter;
import com.justproperty.android.presenters.MenuNavigationPresenter;
import com.justproperty.android.utils.AppBoyManagerInstance;
import com.justproperty.android.utils.Constants;

import org.json.JSONObject;

import java.net.HttpCookie;
import java.util.List;

import javax.inject.Inject;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;


/**
 * Created by ShubhangMalviya on 22/04/16.
 */
public class LoginInteractorImpl implements LoginInteractor, GoogleApiClient.OnConnectionFailedListener {

    @Inject
    Api service;

    @Inject
    SharedPreferenceManager sharedPreferenceManager;

    @Inject
    AppBoyManagerInstance appBoy;

    private final String HEADER_KEY_SESSION_COOKIE = "Set-Cookie";
    private final Context context;
    private static GoogleApiClient googleApiClient;
    private CallbackManager fbCallbackManager;
    private LoginPresenter presenter;

    public LoginInteractorImpl(Context context, LoginPresenter presenter) {
        DaggerInjector.inject(this);
        this.context = context;
        this.presenter = presenter;
    }

    @Override
    public void initializeGoogleSignin() {

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
//                .requestProfile()
//                .requestScopes(new Scope(Scopes.PLUS_ME), new Scope(Scopes.PLUS_LOGIN))
                .requestServerAuthCode(context.getString(R.string.server_client_id))
                .requestIdToken(context.getString(R.string.server_client_id)) // This is required to get the auth 2.0 token in sign in response
                .build();

        if(googleApiClient == null) {

            googleApiClient = new GoogleApiClient.Builder(context)
                    .enableAutoManage((AppCompatActivity) context, this /* OnConnectionFailedListener */)
                    .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                    .build();
        }

    }

    @Override
    public void connectToGoogleApi() {

        if (googleApiClient != null) {

            googleApiClient.connect();
        }
    }

    @Override
    public void sendGoogleLoginRequest(Fragment fragment) {

        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
        fragment.startActivityForResult(signInIntent, Constants.REQUEST_CODE_GOOGLE_SIGN_IN);
    }

    private void preserveLoggedInUserData(UserProfile userProfile)
    {

        sharedPreferenceManager.setUserProfile(userProfile);
        sharedPreferenceManager.setSendEnquiryName(userProfile.name);
        sharedPreferenceManager.setSendEnquiryEmail(userProfile.email);
    }

    /**
     * This function will send the accesss token to server, We will read the cookie in the response and save to local device.
     * It will also update the user data in shared pref, once login confirmed.
     * @param userProfile
     */
    @Override
    public void sendLoginDataToServer(final UserProfile userProfile) {

        Call<UserProfile> enquiryResponseCall = service.sendLoginUserData(userProfile.accessToken, userProfile.socialNetwork);

        enquiryResponseCall.enqueue(new Callback<UserProfile>() {
            @Override
            public void onResponse(final Response<UserProfile> response, Retrofit retrofit) {

                if (response != null && response.body() != null) {

                    setCookieInUserProfile(userProfile, response.headers().get(HEADER_KEY_SESSION_COOKIE));
                    updatedUserObjectWithResponse(userProfile, response.body());
                    preserveLoggedInUserData(userProfile);
                    // When is request is made through LoginCookieManager then the present will be null, as the request will go into background
                    // without any presenter layer. So the following check is perfectly valid
                    if (presenter != null) {
                        presenter.onLoginToServerSuccess(userProfile);
                    }
                } else {

                    if (presenter != null) {

//                        presenter.onLoginToServerSuccess(userProfile);
                        presenter.onLoginToServerFailed(context.getString(R.string.login_to_server_error));
                    }
                }
            }

            @Override
            public void onFailure(Throwable t) {

                if (context == null || ((Activity) context).isFinishing() || presenter == null) {
                    return;
                }

                presenter.onLoginToServerFailed(context.getString(R.string.login_to_server_error));

            }
        });

    }

    /**
     * The server send's email and user id in login response. These details need's to be updated in the
     * user's object.
     * @param userProfile
     * @param responseUser
     */
    private void updatedUserObjectWithResponse(UserProfile userProfile, UserProfile responseUser) {

        userProfile.email = responseUser.email;
        userProfile.id = responseUser.id;
    }


    private void setCookieInUserProfile(UserProfile userProfile, String header) {

        List<HttpCookie> cookies = HttpCookie.parse(header);


        userProfile.sessionCookie = cookies.get(0).getValue();
        /**
         * We take 1 day buffer from the date at which the cookie is going to expire from server.
         * And we update the cookie from server 1 day before it actually going to expire it.
         */
        userProfile.cookieTimeStamp = (System.currentTimeMillis()/1000) + (cookies.get(0).getMaxAge() - Constants.COOKIE_EXPIRY_TIME_BUFFER);
    }

    @Override
    public void handleGoogleSigninResponse(Intent intent) {
        GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(intent);
//        Log.d(TAG, "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            GoogleSignInAccount acct = result.getSignInAccount();
            UserProfile userProfile = new UserProfile();
            userProfile.accessToken = acct.getIdToken();
            userProfile.email = acct.getEmail();
            userProfile.name = acct.getDisplayName();
            if (acct.getPhotoUrl() != null) {
                userProfile.profilePicUrl = acct.getPhotoUrl().toString();
            }
            userProfile.socialNetwork = UserProfile.SOCIAL_LOGIN_GOOGLE;
            googleApiClient.clearDefaultAccountAndReconnect();


            if (TextUtils.isEmpty(userProfile.accessToken) || TextUtils.isEmpty(acct.getEmail())) {
                presenter.onLoginToServerFailed(context.getString(R.string.google_signin_failed));
            } else {
                sendLoginDataToServer(userProfile);
            }
        } else {
            presenter.onLoginToServerFailed(context.getString(R.string.google_signin_failed));
        }
    }

    @Override
    public void handleFacebookSigninResponse(int requestCode, int resultCode, Intent intent) {
        fbCallbackManager.onActivityResult(requestCode, resultCode, intent);
    }

    @Override
    public void initializeFbSignin(LoginButton fbLoginButton) {
        fbLoginButton.setReadPermissions(Constants.FACEBOOK_PERMISSIONS);
        FacebookSdk.sdkInitialize(context.getApplicationContext());
        fbCallbackManager = CallbackManager.Factory.create();
        fbLoginButton.registerCallback(fbCallbackManager, fbLoginCallback);
    }

    private FacebookCallback<LoginResult> fbLoginCallback = new FacebookCallback<LoginResult>() {
        @Override
        public void onSuccess(final LoginResult loginResult) {

            GraphRequest request = GraphRequest.newMeRequest(
                    loginResult.getAccessToken(),
                    new GraphRequest.GraphJSONObjectCallback() {
                        @Override
                        public void onCompleted(JSONObject object,
                                GraphResponse response) {
                            // Application code
                            UserProfile profile = parseJsonToUserObject(loginResult.getAccessToken().getToken(), object);
                            if (profile != null) {
                                sendLoginDataToServer(profile);
                            }
                        }
                    });
            Bundle parameters = new Bundle();
            parameters.putString("fields", "id,name,link,email");
            request.setParameters(parameters);
            request.executeAsync();

        }

        @Override
        public void onCancel() {
            presenter.onLoginToServerFailed(context.getString(R.string.fb_login_canceled));
        }

        @Override
        public void onError(FacebookException error) {
            presenter.onLoginToServerFailed(context.getString(R.string.fb_signin_failed));
        }
    } ;

    private UserProfile parseJsonToUserObject(String token, JSONObject responseJson) {

        if (responseJson == null) {
            presenter.onLoginToServerFailed(context.getString(R.string.fb_signin_failed));
            return null;
        }

        UserProfile userProfile = new UserProfile();
        userProfile.name = responseJson.optString("name");
        userProfile.socialNetwork = UserProfile.SOCIAL_LOGIN_FACEBOOK;
        userProfile.email = responseJson.optString("email");
        userProfile.profilePicUrl = "http://graph.facebook.com/" + responseJson.optString("id") + "/picture?type=large";
        userProfile.accessToken = token;

        if (TextUtils.isEmpty(userProfile.accessToken) || TextUtils.isEmpty(userProfile.email)) {
            presenter.onLoginToServerFailed(context.getString(R.string.fb_signin_failed));
            return null;
        }
        return userProfile;
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        presenter.onLoginToServerFailed(context.getString(R.string.google_signin_failed));
    }



    @Override
    public void disconnectGoogleApiClient()
    {
        if (googleApiClient != null && googleApiClient.isConnected()) {
            googleApiClient.stopAutoManage((FragmentActivity) context);
            googleApiClient.disconnect();
        }
    }

    @Override
    public void setUserToAppBoy(String userId) {

        appBoy.setCurrentUser(context, userId);
    }

    @Override
    public void signOut(final Context context, final MenuNavigationPresenter menuNavigationPresenter) {
       // menuNavigationPresenter.onSignOutSuccess();
        Auth.GoogleSignInApi.signOut(googleApiClient).setResultCallback(
            new ResultCallback<Status>() {
                @Override
                public void onResult(@NonNull Status status) {
                    if(status.isSuccess()) {
                        googleApiClient.stopAutoManage((FragmentActivity) context);
                        googleApiClient.disconnect();
                        googleApiClient = null;
                        menuNavigationPresenter.onSignOutSuccess();
                    }
                }
            });

    }
}
